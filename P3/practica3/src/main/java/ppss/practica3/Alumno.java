/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.practica3;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class Alumno {
    
    public boolean validaNif(String nif) {
        // if (nif.length() != 9 || nif == null) {
        if (nif == null || nif.length() != 9) { //Corregido
            return false;
        }

        String dni = nif.substring(0, 8);
        char letra = nif.charAt(8);

        Pattern pattern = Pattern.compile("[0-9]{8,8}");
        Matcher matcher = pattern.matcher(dni);
        String letras = "TRWAGMYFPDXBNJZSQVHLCKE";
        long ldni = 0;

        try {
            ldni = Long.parseLong(dni);
            if (ldni < 0) { //Corregido añadido
                return false;
            }
        } catch (NumberFormatException e) {
            return false;
        }

        int indice = (int) (ldni % 23);
        char letraEsperada = letras.charAt(indice);
        return matcher.matches() && letra == letraEsperada;
    }
}
