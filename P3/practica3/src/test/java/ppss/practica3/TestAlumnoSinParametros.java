/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.practica3;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class TestAlumnoSinParametros {
    String nif;
    boolean resultadoEsperado, resultadoReal ;
    Alumno alu;
    
    public TestAlumnoSinParametros() {
    }
    
    @Before
    public void setUp() {
        alu = new Alumno();
    }
    
    @After
    public void tearDown() {
        alu = null;
    }
   @Test
    public void TestAlumnosinParametrosC1() {
        nif = "nif";
        resultadoEsperado = false;
        
        resultadoReal = alu.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void TestAlumnosinParametrosC2() {
        nif = "123";
        resultadoEsperado = false;
        
        resultadoReal = alu.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void TestAlumnosinParametrosC3() {
        nif = "123456AA";
        resultadoEsperado = false;
        
        resultadoReal = alu.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void TestAlumnosinParametrosC4() {
        nif = "-12345678";
        resultadoEsperado = false;
        
        resultadoReal = alu.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void TestAlumnosinParametrosC5() {
        nif = "00000000T";
        resultadoEsperado = true;
        
        resultadoReal = alu.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
}
