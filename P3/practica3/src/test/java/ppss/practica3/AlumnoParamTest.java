/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.practica3;

import java.util.Arrays;
import java.util.Collection;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author ppss
 */

    
    @RunWith(Parameterized.class)
public class AlumnoParamTest {
        @Parameterized.Parameters(name =
                "Caso C{index}: validaNif({0}) = {1}")
        public static Collection<Object[]> data() {
            return Arrays.asList(new Object[][]{
                    {"1234", false},
                    {"1A234567A", false},
                    {"123456789", false},
                    {"12345678A", false},
                    {"45839623W", true}
            });
        }
    private String nif;
    private boolean esperado;
    private Alumno alu;
    @BeforeClass
    public static void intialState(){}
    @AfterClass
    public static void initialState(){}

    @Before
    public void declaracion(){alu = new Alumno();}
    @After
    public void fin(){}

    public AlumnoParamTest(String nif, boolean esperado){
        this.nif = nif;
        this.esperado = esperado;
    }
    @Test
    public void testValidaNif(){
        boolean real = alu.validaNif(nif);
        assertEquals(esperado, real);
    }
}
    

