/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.practica3;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;


/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class TestAlumnoConParametros {
    @Parameterized.Parameters
    (name = "Caso C{index}: validaNif({0}) = {1}")
    
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"nif",false},
            {"123",false},
            {"123456AA",false},
            {"-12345678",false},
            {"00000000X",false},
            {"00000000T",true}
        });       
    }
    
    private String nif;
    private boolean resultadoEsperado;
    Alumno alu = new Alumno();
    
    public TestAlumnoConParametros(String nif, boolean resultadoEsperado) {
        this.nif = nif;
        this.resultadoEsperado = resultadoEsperado;
    }
    
    @Test
    public void testvalidaNif() {
        boolean resultadoReal = alu.validaNif(nif);
        assertEquals(resultadoEsperado, resultadoReal);
    }

   
}
