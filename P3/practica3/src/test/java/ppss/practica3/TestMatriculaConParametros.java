/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.practica3;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.Matricula;

@RunWith(Parameterized.class)
public class TestMatriculaConParametros {
    @Parameterized.Parameters
        (name = "Caso C{index}: calculaTasaMatricula({0},{1},{2}) = {3}")
    
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
                {20,false,true,2000},
                {70,false,true,250},
                {20,true,true,250},
                {20,false,false,500},
                {60,false,true,400},
                {51,true,false,400} //mio

        });
    }

    private int edad;
    private boolean familiaNumerosa;
    private boolean repetidor;
    private float resultadoEsperado;
    Matricula mat = new Matricula();
    
    public TestMatriculaConParametros(int edad, boolean familiaNumerosa, boolean repetidor, float resultadoEsperado) {
        this.edad = edad;
        this.familiaNumerosa = familiaNumerosa;
        this.repetidor = repetidor;
        this.resultadoEsperado = resultadoEsperado;
    }
    
    @Test
    public void testCalculaTasaMatricula() {
        float resultadoReal = mat.calculaTasaMatricula(edad, familiaNumerosa, repetidor);
        assertEquals(resultadoEsperado, resultadoReal, 0.002f);
    }
    
    
}
