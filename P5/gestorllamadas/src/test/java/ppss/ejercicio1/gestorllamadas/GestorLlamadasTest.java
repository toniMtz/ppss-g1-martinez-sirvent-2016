/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio1.gestorllamadas;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class GestorLlamadasTest {
    
    int resultadoReal, resultadoEsperado, hora, minutos;
    
    public GestorLlamadasTest() {
    }

    @Test
    public void testCalculaConsumoC1() {
        minutos = 10;
        resultadoEsperado = 208;
        HoraActualStub stub = new HoraActualStub();
        stub.setHoraActual(15);
        resultadoReal = (int) stub.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
    @Test
    public void testCalculaConsumoC2() {
        minutos = 10;
        resultadoEsperado = 105;
        HoraActualStub stub = new HoraActualStub();
        stub.setHoraActual(22);
        resultadoReal = (int) stub.calculaConsumo(minutos);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    
}
