/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio2;

/**
 *
 * @author ppss
 */
public class TestableGestorLlamadas extends GestorLlamadas {
    private  int hora;

    @Override
    public Calendario getCalendario() {
        CalendarioStub c = new CalendarioStub();
        c.setHoraActual(hora);
        return c;
    }

    public void setHoraActual(int hora) {
        this.hora = hora;
    }

}
