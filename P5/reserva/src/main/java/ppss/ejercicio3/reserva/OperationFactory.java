/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.reserva;

/**
 *
 * @author ppss
 */
public class OperationFactory {
    private static IOperacionBO operacion = null;
    
    public static IOperacionBO Create() {
        if (operacion != null) {
            return operacion;
        } else {
            return new Operacion();
        }
    }

    /**
     * @param aOperacion the operacion to set
     */
    public static void setOperacion(IOperacionBO aOperacion) {
        operacion = aOperacion;
    }
    
    
}
