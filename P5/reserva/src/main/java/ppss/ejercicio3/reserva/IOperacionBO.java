/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.reserva;

import ppss.ejercicio3.reserva.excepciones.*;
/**
 *
 * @author ppss
 */
public interface IOperacionBO {
    
    public void setAccesoBD(boolean AccesoBD);
    public void setValidaSocio(boolean ValidaSocio);
    public void setValidaISBN(boolean ValidaISBN);
    
    public void operacionReserva(String socio, String isbn)
            throws IsbnInvalidoException, JDBCException, SocioInvalidoException;
}

    