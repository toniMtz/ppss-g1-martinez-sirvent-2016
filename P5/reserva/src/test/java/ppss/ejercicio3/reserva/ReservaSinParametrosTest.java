/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.reserva;

import org.junit.Test;
import static org.junit.Assert.*;
import ppss.ejercicio3.reserva.excepciones.ReservaException;

/**
 *
 * @author ppss
 */
public class ReservaSinParametrosTest {
    
    public ReservaSinParametrosTest() {
    }

    /**
     * Test of compruebaPermisos method, of class Reserva.
     */
    @Test
    public void testCompruebaPermisos() throws Exception {
        String[] isbn = new String[] {"1111"};

        PermisosStub p = new PermisosStub();
        p.setPermiso(false);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(true);
         io.setValidaSocio(true);
         io.setValidaISBN(false);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva("xxxx","xxxx","Luis",isbn);
            fail("La exepcion no se lanzo");
        }catch (ReservaException e) {
            assertEquals("ERROR de permisos; " ,e.getMessage());
        } 
    }
    @Test
    public void testRealizaReserva() throws Exception {
        String[] isbn = new String[] {"1111","2222"};

        PermisosStub p = new PermisosStub();
        p.setPermiso(true);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(true);
         io.setValidaSocio(true);
         io.setValidaISBN(true);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva("ppss","ppss","Luis",isbn);
        }catch (ReservaException e) {
            fail("No deberian lanzarse exceptions");
        } 
    }
    
    @Test
    public void testOperacionReservaISBN() throws Exception {
        String[] isbn = new String[] {"3333"};

       PermisosStub p = new PermisosStub();
        p.setPermiso(true);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(true);
         io.setValidaSocio(true);
         io.setValidaISBN(false);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva("ppss","ppss","Luis",isbn);
            fail("La exepcion no se lanzo");
        }catch (ReservaException e) {
            assertEquals("ISBN invalido:3333; " ,e.getMessage());
        }
    }
    
    @Test
    public void testOperacionReservaSocio() throws Exception {
        String[] isbn = new String[] {"1111"};

       PermisosStub p = new PermisosStub();
        p.setPermiso(true);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(true);
         io.setValidaSocio(false);
         io.setValidaISBN(true);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva("ppss","ppss","Pepe",isbn);
            fail("La exepcion no se lanzo");
        }catch (ReservaException e) {
            assertEquals("SOCIO invalido; " ,e.getMessage());
        }
    }
    
    @Test
    public void testOperacionReservaConnexion() throws Exception {
        String[] isbn = new String[] {"1111"};

       PermisosStub p = new PermisosStub();
        p.setPermiso(true);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(false);
         io.setValidaSocio(true);
         io.setValidaISBN(true);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva("ppss","ppss","Luis",isbn);
            fail("La exepcion no se lanzo");
        }catch (ReservaException e) {
            assertEquals("CONEXION invalida; " ,e.getMessage());
        }
    }
}

  