/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.reserva;

/**
 *
 * @author ppss
 */
public class PermisosStub extends Reserva{
    private boolean permiso;

    /**
     * @return the permiso
     */
    
    @Override
    public boolean compruebaPermisos(String login, String password, Usuario tipoUsu) {
        return isPermiso();
    }
    
    
    public void setPermiso(boolean permiso) {
        this.permiso = permiso;
    }

    public boolean isPermiso() {
        return permiso;
        //throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
