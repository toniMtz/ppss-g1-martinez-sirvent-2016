/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.reserva;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import ppss.ejercicio3.reserva.excepciones.ReservaException;

/**
 *
 * @author ppss
 */
@RunWith(Parameterized.class)
public class ReservaConParametrosTest {
    @Parameterized.Parameters
    (name = "Caso C{index}: realizaReserva({0},{1},{2},{3},{4},{5},{6},{7}) = {8}")
    public static Collection<Object[]> data() {
        return Arrays.asList(new Object[][]{
            {"xxx","xxx","Luis",false,true,true,true,new String[] {"1111"},"ERROR de permisos; "},
            {"ppss","ppss","Luis",true,false,true,true,new String[] {"3333"},"ISBN invalido:3333; "},
            {"ppss","ppss","Luis",true,true,false,true,new String[] {"1111"},"SOCIO invalido; "},
            {"ppss","ppss","Luis",true,true,true,false,new String[] {"1111"},"CONEXION invalida; "}
        });       
    }
    
    private String login, pass, idSocio, resultadoEsperado;
    private boolean accesoBD, validaSocio, validaISBN, permiso;
    private String [] isbn;
    
    public ReservaConParametrosTest(String login, String pass, String idSocio, boolean permiso, boolean validaISBN, boolean validaSocio, 
            boolean accesoBD, String[] isbn, String resultadoEsperado) {
        
        this.login = login;
        this.pass = pass;
        this.idSocio = idSocio;
        this.resultadoEsperado = resultadoEsperado;
        this.accesoBD = accesoBD;
        this.validaISBN = validaISBN;
        this.validaSocio = validaSocio;
        this.permiso = permiso;
        this.isbn = isbn;
    }

   
    @Test
    public void testRealizaReservaException() throws Exception {
        PermisosStub p = new PermisosStub();
        p.setPermiso(permiso);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(accesoBD);
         io.setValidaSocio(validaSocio);
         io.setValidaISBN(validaISBN);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva(login, pass,idSocio, isbn);
            fail("La exepcion no se lanzo");
        }catch (ReservaException e) {
            assertEquals(resultadoEsperado ,e.getMessage());
        } 
    }
    
    @Test
    public void testRealizaReservaOK() throws Exception {
        
         String[] isbn = new String[] {"1111","2222"};

        PermisosStub p = new PermisosStub();
        p.setPermiso(true);
         IOperacionBO io = new OperationStub();
         io.setAccesoBD(true);
         io.setValidaSocio(true);
         io.setValidaISBN(true);
       
       OperationFactory fact = new OperationFactory();
        fact.setOperacion(io);
         try {
            p.realizaReserva("ppss","ppss","Luis",isbn);
        }catch (ReservaException e) {
            fail("No deberian lanzarse exceptions");
        } 
    }
}

  