/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss.ejercicio3.reserva;

import ppss.ejercicio3.reserva.excepciones.IsbnInvalidoException;
import ppss.ejercicio3.reserva.excepciones.JDBCException;
import ppss.ejercicio3.reserva.excepciones.SocioInvalidoException;

/**
 *
 * @author ppss
 */
public class OperationStub extends Operacion{
    
    private boolean validaSocio;
    private boolean validaISBN;
    private  boolean accesoBD;
    
    public void operacionReserva(String socio, String isbn) throws IsbnInvalidoException, JDBCException, SocioInvalidoException {
       if(!isValidaISBN()){
            throw new IsbnInvalidoException();
        }
        if(!isValidaSocio()){
            throw new SocioInvalidoException();
        }
        if(!isAccesoBD()) {
            throw new JDBCException();
        }
    }

    public boolean isAccesoBD() {
        return accesoBD;
    }

    /**
     * @param accesoBD the accesoBD to set
     */
    public void setAccesoBD(boolean accesoBD) {
        this.accesoBD = accesoBD;
    }

    /**
     * @return the validaSocio
     */
    public boolean isValidaSocio() {
        return validaSocio;
    }

    /**
     * @param validaSocio the validaSocio to set
     */
    public void setValidaSocio(boolean validaSocio) {
        this.validaSocio = validaSocio;
    }

    /**
     * @return the validaISBN
     */
    public boolean isValidaISBN() {
        return validaISBN;
    }

    /**
     * @param validaISBN the validaISBN to set
     */
    public void setValidaISBN(boolean validaISBN) {
        this.validaISBN = validaISBN;
    }
}
