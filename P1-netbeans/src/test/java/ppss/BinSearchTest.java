/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ppss;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ppss
 */
public class BinSearchTest {
    int key;
    int[] anArray = { 0, 1, 2, 3, 4};
    BinSearch Bs= new BinSearch();
    int resultadoReal;
     int resultadoEsperado;
     
     @Test
    public void testBusquedaC1() {
        key = 2;
        resultadoEsperado = key;
        resultadoReal = Bs.search(key, anArray);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void testBusquedaC2() {
        key = 3;
        resultadoEsperado = key;
        resultadoReal = Bs.search(key, anArray);
        assertEquals(resultadoEsperado, resultadoReal);
    }
    @Test
    public void testBusquedaC3() {
        key = 5;
        resultadoEsperado = -1;
        resultadoReal = Bs.search(key, anArray);
        assertEquals(resultadoEsperado, resultadoReal);
    }
}

