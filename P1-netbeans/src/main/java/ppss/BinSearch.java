
package ppss;

public class BinSearch {
    
    public int search (int key, int[] elemArray) {
        int bottom = 0 ;
        int top = elemArray.length - 1 ;
        int mid ;
        int index = -1;
        
        while ( bottom <= top ) {
            mid = (top + bottom) / 2 ;
            
            if (elemArray[mid] == key) {
                index = mid;
                break;
            } else {
                if (elemArray [mid] < key)
                    bottom = mid + 1 ;
                else
                    top = mid - 1 ;
            }
       } //while loop
        return index;
    }
} //BinSearch

       
        
    


